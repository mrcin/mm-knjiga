%% LyX 1.6.4 created this file.  For more info, see http://www.lyx.org/.
%% Do not edit unless you really know what you are doing.
\documentclass[12pt,slovene]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{amsthm}

\makeatletter
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Textclass specific LaTeX commands.
\theoremstyle{plain}
\newtheorem{thm}{Theorem}
\theoremstyle{definition}
\newtheorem{example}[thm]{Example}

\makeatother

\usepackage{babel}

\begin{document}
\textbf{Naloga:} Poišči približno rešitev robnega problema za Poissonovo
enačbo \[
\nabla^{2}u(r,\phi)=f(r)\]
 na krogu z radijem $r_{0}$ in z robnim pogojem $u(r_{0},\phi)=konst.=u_{r}$.
Brez škode lahko predpostavimo, da je $u_{r}=0$. Enačba je podana
v polarnih koordinatah $(r,\phi)$. Poseben primer, ko je $f(r)=0$,
imenujemo Laplaceova enačba. Laplaceov operator $\nabla^{2}$ je v
kartezičnih koordinatah $(x,y)$ definiran kot \[
\nabla^{2}u(x,y)=\frac{\partial^{2}u}{\partial x^{2}}+\frac{\partial^{2}u}{\partial y^{2}}.\]


\textbf{Primeri:} Poissonova enačba opisuje veliko stacionarnih pojavov,
na primer 
\begin{enumerate}
\item potencial električnega polja pri dani porazdelitvi naboja 
\item hitrost toka nestisljive tekočine v cevi 
\item obliko opne, ki se povesi pod lastno težo 
\item obliko opne, na katero deluje tlačna razlika 
\item ... 
\end{enumerate}
V nadaljevanju si bomo podrobneje ogledali primer milnega mehurčka,
napetega na krožno zanko, ki se povesi pod lastno težo.

\textbf{Rešitev:} Nalogo bomo rešili tako, da bomo z metodo končnih
razlik (diferenc) Poissonovo enačbo prevedli na sistem linearnih enačb.

Najprej poenostavimo originalno enačbo. Preprosta naloga iz analize
2 je zapisati Laplaceov operator v polarnih koordinatah \[
\nabla^{2}u(r,\phi)=\frac{1}{r}\frac{\partial}{\partial r}\left(r\frac{\partial u}{\partial r}\right)+\frac{1}{r^{2}}\frac{\partial^{2}u}{\partial\phi^{2}}\]
 in ni težko videti, da je rešitev odvisna le od $r$, če je desna
stran enačbe odvisna le od $r$. Tako je $u(r,\phi)=u(r)$ in $\frac{\partial^{2}u}{\partial\phi^{2}}=0$.
Poissonova enačba za $u(r)$ postane navadna diferencialna enačba
drugega reda \begin{equation}
u''(r)+\frac{1}{r}u'(r)=f(r),\quad u(r_{0})=0\,\mathrm{in}\, u'(0)=0\label{problem}\end{equation}
 Poleg robnega pogoja $u(r_{0})=0$ moramo dodati še pogoj $u'(r)|_{r=0}=0$,
sicer rešitev ne bo gladka v izhodišču.


\subsection*{Metoda končnih razlik}

V splošnem takega problema ne znamo rešiti, saj navadno ne poznamo
splošne rešitve zgornje diferencialne enačbe. Zato nam ponavadi preostane
le še numerično reševanje. Kaj pomeni numerično rešiti problem (\ref{problem})?
Nič drugega, kot na intervalu $[0,r_{0}]$ poiskati numerične približke
$u_{i}\approx u(x_{i})$, $i=1,2,\dots,n$, pri čemer je \[
0=:x_{0}<x_{1}<\cdots<x_{n}<x_{n+1}:=r_{0}.\]
 Več kot je izbranih točk $x_{i}$, bolj natančen opis funkcije $u$
s približki $u_{i}$ pričakujemo.

Zaradi enostavnosti ponavadi predpostavimo, da je $x_{i+1}-x_{i}=h$
za vse $i$. Drugače povedano, točke $x_{i}$ naj bodo \emph{ekvidistantne}.
Da bi lahko reševali problem (\ref{problem}), moramo najprej aproksimirati
$u''(x_{i})$ in $u'(x_{i})$. To naredimo s t.i. drugimi diferencami
\[
u''(x_{i})=\frac{u(x_{i-1})-2u(x_{i})+u(x_{i+1})}{h^{2}}+{\rm napaka}=\frac{u_{i-1}-2u_{i}+u_{i+1}}{h^{2}}+{\rm napaka}\]
 oziroma s prvimi diferencami \[
u'(x_{i})=\frac{u_{i+1}-u_{i-1}}{2h}+\mathrm{napaka}.\]
 Če zanemarimo napako, ki je sorazmerna s $h^{2}$, morajo veljati
naslednje enačbe \[
(1-\frac{h}{2x_{i}})u_{i-1}-2u_{i}+(1+\frac{h}{2x_{i}})u_{i+1}=h^{2}f(x_{i}),\quad i=0,1,2,\dots,n.\]
 Ker morata biti ozpolnjena robna pogoja, sta prva in zadnja enačba
malce drugačni, namreč \begin{eqnarray*}
-2u_{0}+2u_{1} & = & h^{2}f(0)\\
(1-\frac{h}{2x_{n}})u_{n-1}-2u_{n} & = & h^{2}f(x_{n}).\end{eqnarray*}
 Tako dobimo sistem enačb \begin{equation}
A\mathbf{u}=\mathbf{b},\label{sistem}\end{equation}
 kjer je

\[
A=\left[\begin{array}{ccccccc}
-2 & 2 & 0 & 0 & \cdots & 0 & 0\\
(1-\frac{h}{2x_{1}}) & -2 & (1+\frac{h}{2x_{1}}) & 0 & \cdots & 0 & 0\\
0 & (1-\frac{h}{2x_{2}}) & -2 & (1+\frac{h}{2x_{2}}) & \cdots & 0 & 0\\
\vdots & \vdots & \vdots & \vdots & \vdots & \vdots & \vdots\\
0 & 0 & 0 & 0 & \cdots & (1-\frac{h}{2x_{n}}) & -2\end{array}\right],\]
 \[
\mathbf{b}=h^{2}\begin{bmatrix}f(0)\\
f(x_{1})\\
f(x_{2})\\
\vdots\\
f(x_{n-1})\\
f(x_{n})-y_{b}\end{bmatrix}\]
 in $\mathbf{u}=[u_{0},\dots,u_{n}]^{T}$ vektor neznanih približkov.\\
 Sistem (\ref{sistem}) je tridiagonalen, zato ga v primeru, ko
je delilnih točk $x_{i}$ zelo veliko (torej, ko je $n$ velik), rešujemo
tako, da ne predstavimo cele matrike $A$, pač pa le njene tri glavne
diagonale. 
\begin{example}
Izračunaj obliko milnice napete na krožno zanko, ki se povesi pod
lastno težo. Predpostavi, da je $r_{0}=1$ in reši naslednja primera.
\begin{enumerate}
\item Debelina milnice je konstantna. Desna stran Poissonove enačbe je konstantna
$f(r)=konst.=1$. 
\item Debelina milnice se spreminja sorazmerno z $1-r^{2}$. Desna stran
$f(r)$ je torej sorazmerna $1-r^{2}$. 
\end{enumerate}
\end{example}

\end{document}


function nicle = nicla(x,y)
% funkcija nicle = nicla(x,y) poišče približke za ničle tabelirane funkcije
% x ... tabela neodvisne spremenljivke
% y ... tabela funkcijskih vrednosti
% Rezultat:
% nicle ... tabela približkov za ničle
indeksi = find(y(1:end-1).*y(2:end)<0);
nicle = [];
for i=1:length(indeksi)
  =<\coderef{presek daljice}
  nicle = [nicle x0];
end

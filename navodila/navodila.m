
%}
x = 1:0.5:10;
y = sin(x);
plot(x,y,'-*');
grid on;
print -dpstex "tabela-sin.tex";
%{

%}
find(y==0)
%{

%}
latex(y,'taby.tex')
%{

%}
find(abs(y)<1e-10)
%{

%}
diary 'predznak.txt'
indeks = find(y(1:end-1).*y(2:end)<0)
% pogledamo le prvo ničlo
[y(indeks(1)),y(indeks(1)+1)]
diary off
%{

%}
nicle = nicla(x,y);
%nicle narisemo na isti graf
hold on
plot(nicle,zeros(size(nicle)),'*r')
print -dpstex 'nicle.tex'
%{

%RESI3 resevanje tridiagonalnega sistema, predstavljenega
%samo s tremi vektorji in desno stranjo
%x=RESI3(M,b)
%x je resitev sistema, M je matrika diagonal dim. nx3, v prvem
%stolpcu je spodnja obdiagonala (nicla na mestu (1,1)),
% v drugem diagonala, v tretjem pa zgornja obdiagonala (nicla
%na mestu (n,3)
%b je desna stran sistema
function x=resi3(M,b);
  n=length(b); %dimezija problema
  x=zeros(n,1);
  %unicevanje spodnje obdiagonale
  for i=2:n
    k=M(i,1)/M(i-1,2);
    M(i,2)=M(i,2)-k*M(i-1,3);
    b(i)=b(i)-k*b(i-1);
  end
  %obratno vstavljanje
  x(n)=b(n)/M(n,2);
  for i=n-1:-1:1
    x(i)=(b(i)-x(i+1)*M(i,3))/M(i,2);
  end

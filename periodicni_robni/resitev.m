%RESITEV vrne priblizke za resitev robnega problema
%[x,ynum]=RESITEV(f,n,a,b,ya,yb);
%x je zaporedje x_i, ynum priblizki y_i
%f je funkcija f na desni strani, n notranjih tock x_i
%a in b sta krajisci intervala, ya in yb vrednosti v teh krajiscih
function [x,ynum]=resitev(f,n,a,b,ya,yb)
  x=linspace(a,b,n+2)';
  h=x(2)-x(1);
  %gradimo matriko M
  enke=ones(n,1);
  M=[[0;enke(1:n-1,1)] -2*enke [enke(1:n-1,1);0]];
  ynum=zeros(n+2,1);
  ynum(1,1)=ya;
  ynum(n+2,1)=yb;
  b=zeros(n,1);
  b(1,1)=h^2*feval(f,x(2))-ya;
  b(n,1)=h^2*feval(f,x(n+1))-yb;
  b(2:n-1,1)=h^2*feval(f,x(3:n));
  ynum(2:n+1)=resi3(M,b);

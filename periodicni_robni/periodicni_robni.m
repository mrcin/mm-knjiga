%PERIODICNI_ROBNI vrne priblizke za resitev periodicnega robnega problema
%za enacbo drugega reda
%	y''(x) = f(x)
%[x,ynum]=PERIODICNI_ROBNI(f,n,a,b,ya,yb);
%x je zaporedje x_i, ynum priblizki y_i
%f je funkcija f na desni strani, n notranjih tock x_i
%a in b sta krajisci intervala
function [ynum,x]=periodicni_robni(f,a,b,n,eps,maxk)
  x = linspace(a,b,n+1);
  h = x(2)-x(1);
  b = h^2*feval(f,x);
  y = zeros(size(x));
  % razsirimo y
  y = [y(end) y y(1)];
  k=0; napaka = inf;
  % uporabimo Gauss-Seidlovo iteracijo
  while (napaka > eps) & (k<maxk)
    y_prej = y;
    for i=2:(length(y)-1)
      y(i) = -0.5*(b(i-1)-y(i-1)-y(i+1));
    end
    y(1)=y(end-1); y(end)=y(2); 
    napaka = max(abs(y-y_prej));
    k=k+1;
  end
  k
  if k>=maxk
    ynum=NaN;
  else
    ynum = y(2:end-1);
  end

function [Y,T] = rk4(fun,t0,Y0,tk,n)
% funkcija y = rk4(fun,t0,y0,tk,n)
% poišče rešitev začetnega problema
% y'(t)=fun(t,y), y(t0)=y0
% z metodo RK4 na intervalu [t0,tk]
% s korakom (tk-t0)/n
  T=linspace(t0,tk,n+1);
  h=T(2)-T(1);
  Y=zeros(length(Y0),n+1);
  Y(:,1)=[Y0];
  for i=1:n
    k1=h*feval(fun,T(i),Y(:,i));
    k2=h*feval(fun,T(i)+h/2,Y(:,i)+k1/2);
    k3=h*feval(fun,T(i)+h/2,Y(:,i)+k2/2);
    k4=h*feval(fun,T(i)+h,Y(:,i)+k3);
    Y(:,i+1)=Y(:,i)+(k1+2*k2+2*k3+k4)/6;
  end

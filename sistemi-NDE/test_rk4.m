# Program za testiranje metode Runge-Kutta reda 4.
# 
# Metoda rk4 poišče približek za začetni problem
# za rešuje navadno diferencialno enačbo ali sistem
# enačb oblike 
# y'(x) = f(x,y)
# Metodo bomo preiskusili na Airijevi enačbi:
# y''(t) = t*sin(y(t)
# katere splošna rešitev je dana z Airyjevimi
# funkcijami
# y(t) = C1*Ai(t) + C2*Bi(t).

airy_de = inline('[y(2);x*y(1)]','x','y');
# zacetni pogoji
t0 = 0; y0 = [1;0];
tk = -3;
# določimo partikularno rešitev, tako da 
# izračunamo konstanto C
A = [airy(0,0) airy(2,0);
		airy(1,0) airy(3,0)];
C = A\y0;
# priblizna resitev z rk4
napake=[]; koraki=[];
for k=10:20:800
	# približna rešitev
	[y,t]=rk4(airy_de,t0,y0,tk,k);
	# točna rešitev
	y_p = C(1)*airy(0,t) + C(2)*airy(2,t);
	napake = [napake max(abs(y_p - y(1,:)))];
	koraki = [koraki k];
end
# narišemo graf napake
plot(log10(koraki),log10(napake))
xlabel("koraki: (10^x)")
ylabel("velikostni red napake")
title("Napaka metode Runge-Kutta 4 (log-log graf)")
% izračunamo še naklon premice, ki mora biti 4
p = polyfit(log10(koraki),log10(napake),1);
printf('Numeričen red metode: %f\n', abs(p(1)))
if abs(p(1)) < 3
	printf('Premajhen red metode: %f\n',abs(p(1)))
end 

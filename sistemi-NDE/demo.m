% začetni pogoji
t0=0;
R0=6356;
h0=340;
v0=27700/3600;
theta0=51*pi/180;
Y0=[0; R0 + h0; 0; v0*[cos(theta0); 0; sin(theta0)]]
% izračunamo trajektorije
Ym=rk4('monopol',t0,Y0,30000,1000);
Yk=rk4('kvadrupol',t0,Y0,30000,1000);
% narišemo projekcijo na ravnino x,y
axis('equal');
xlabel('Brez popravka');
plot(Ym(2,:),Ym(1,:),'r');
figure;
xlabel('S kvadrupolnim popravkom');
plot(Yk(2,:),Yk(1,:),'b');


function dY = lotka_volterra(t, Y)
# funkcija dY = lotka_volterra(t, Y)
# izračuna desne strani sistema Lotka-Volterra
# x'(t)= x(alfa - beta*y)
# y'(t)= -y(gama - delta*x)

x=Y(1); y=Y(2);
# desne strani so podane z vektorjem
dY = [ x*(alfa - beta*y); -y*(gama - delta*x)]

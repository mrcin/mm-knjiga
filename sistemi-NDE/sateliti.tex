%% LyX 1.6.4 created this file.  For more info, see http://www.lyx.org/.
%% Do not edit unless you really know what you are doing.
\documentclass[a4paper,oneside,slovene]{book}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{listings}
\setcounter{secnumdepth}{3}
\setcounter{tocdepth}{3}
\usepackage{babel}

\usepackage{textcomp}
\usepackage{amsthm}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage[unicode=true, pdfusetitle,
 bookmarks=true,bookmarksnumbered=false,bookmarksopen=false,
 breaklinks=false,pdfborder={0 0 1},backref=false,colorlinks=false]
 {hyperref}

\makeatletter

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LyX specific LaTeX commands.
\newcommand{\lyxmathsym}[1]{\ifmmode\begingroup\def\b@ld{bold}
  \text{\ifx\math@version\b@ld\bfseries\fi#1}\endgroup\else#1\fi}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Textclass specific LaTeX commands.
\theoremstyle{plain}
\newtheorem{thm}{Theorem}
\theoremstyle{definition}
\newtheorem{example}[thm]{Example}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% User specified LaTeX commands.
\usepackage{verbatim}
\newcommand{\sourcefile}[1]{%
\begin{quote}%
\small
\verbatiminput{#1}%
\end{quote}%
}%

\makeatother

\begin{document}

\chapter{Navadne diferencialne enačbe}

\global\long\def\rmd{\mathrm{d}}
Veliko pojavov lahko opišemo z navadnimi diferencialnimi enačbami.
Sem spadajo predvsem dinamični sistemi, ki opisujejo spremembe opazljivk
v odvisnosti od časa. Diferencialne enačbe omogočajo, da za opis uporabimo
„lokalne zakone“, ki so relativno enostavni. Najlepši primer „lokalnega
zakona“ je drugi Newtonov zakon, ki opisuje gibanje točkastega telesa
pod vplivom sile in sicer preavi, da je pospešek sorazmeren sili,
ki deluje na točkasto telo. Zakon lahko zapišemo z diferencialno enačbo
2. reda\begin{equation}
a=\frac{\rmd^{2}x(t)}{\rmd t^{2}}=\frac{F}{m}.\label{eq:Newton2}\end{equation}
Če želimo določiti trajektorijo točkastega telesa, moramo poznati
še začetni položaj in začetno hitrost v določenem trenutku. to pomeni,
da imamo poleg diferencialne enačbe \eqref{eq:Newton2} podana še
dva začetna pogoja\begin{align*}
x(t_{0}) & =x_{0}\\
\dot{x}(t_{0}) & =v_{0}.\end{align*}
Tako zastavljeni problem ima enolično rečitev in ga imenujemo \emph{začetni
problem }oziroma \emph{Cauchyjeva naloga }za diferencialno enačbo\emph{.}
Omeniti velja, da za diferencialne enačbe ne rešujemom vedno začetnega
problema. Velikokrat je treba poiskati rešitev \emph{ robnega problema,}
ki se ga rešuje povsem drugače kot začetnega. Predtavljajmo si navaden
navpičen met žogice. Vprašanje, ki ga zastavlja robni problem je,
kako moramo vreči žogico, da bo po določenem času padla na tla. Namesto
začetnih pogojev imamo sedaj dva robna pogoja\[
x(0)=0\quad\text{in }x(t_{0})=0.\]
V tem poglavju se bomo osredotočili na reševanje začetnega problema,
robne probleme si bomo ogledali v naslednjem poglavju.


\section{Numerične metode za začetni problem}

Metode za reševanje začetnega problema temeljijo na preprosti ideji.
Sposodimo si fizikalne izraze in si poglejmo, kaj v resnici pove diferencialna
enačba\begin{equation}
y'(t)=f(t,y(t))\label{eq:DE1}\end{equation}
o obnašanju opazljivke $y(t)$. Če poznamo vrednosti $t$ in $y$,
lahko iz enačbe \eqref{eq:DE1} izračunamo vrednost odvoda. Pri začetnem
problemu imamo v času $t_{0}$ podan začetni pogoj \[
y(t_{0})=y_{0}\]
in lahko iz enačbe \eqref{eq:DE1} izračunamo tudi vrednost odvoda
$y'(t_{0})$. Diferencialna enačba nam torej za $t_{0}$ pove, kako
hitro se vrednost $y(t)$ spreminja. Za dovolj majhen $h$ lahko torej
dovolj dobro ocenimo vrednost funkcije v $y(t_{0}+h)$. Ko imamo približek
za vrednost $y(t_{0}+h)$ lahko zopet iz diferencialne enačbe izračunamo
odvod v $t_{0}+h$ in spet ocenimo vrednost v bližnji točki $y(t_{0}+2h)$.
Tako lahko korak za korakom računamo nove približke, ki vedno bolj
oddaljeni od začetnega časa $t_{0}$. Korak je lahko fiksen ali pa
ga adaptivno spreminjamo glede na oceno napake. V nadaljevanju si
bomo ogledali metodo Runge-Kutta reda 4, ki jo bomo uporabljali za
numerične izračune v tej knjigi.


\subsection{Metoda Runge-Kutta 4}

V knjigi bomo uporabili metodo \href{http://en.wikipedia.org/wiki/Runge-Kutta}{metodo
Runge-Kutta} reda 4. Metoda je relativno enostavna, po drugi strani
pa je dovolj visokega reda, da lahko z njo rešujemo tudi bolj zahtevne
probleme. Rešujemo začetni problem za diferencialno enačbo 1. reda\[
y'(t)=f(t,y)\]
z začetnim pogojem $y(t_{0})=y_{0}$. Metoda računa zaporedne približke
$y_{k}$ v ekvidistančnih točkah\[
t_{0},t_{0}+h,t_{0}+2h,\ldots.\]
Naslednji približek $y_{k}$ izračunamo iz prejšnjega s formulo\begin{align*}
k_{1} & =h\, f(t_{k-1},y_{k-1})\\
k_{2} & =h\, f(t_{k-1}+\frac{h}{2}+y_{k-1}+\frac{k_{1}}{2})\\
k_{3} & =h\, f(t_{k-1}+\frac{h}{2}+y_{k-1}+\frac{k_{2}}{2})\\
k_{4} & =h\, f(t_{k-1}+h+y_{k-1}+k_{3})\\
y_{k} & =y_{k-1}+\frac{1}{6}(k_{1}+k_{2}+k_{3}+k_{4}).\end{align*}
Vse skupaj lahko enostavno zapakiramo v \emph{octave }funkcijo
\begin{quote}
\sourcefile{rk4.m}
\end{quote}

\subsection{Metode prirejene za sisteme NDE}

Rešujemo torej začetni problem za sistem navadnih diferencialnih enačb
prvega reda: \begin{eqnarray*}
y_{1}'(t) & = & f_{1}(t,y_{1},\ldots,y_{n})\\
y_{2}'(t) & = & f_{2}(t,y_{1},\ldots,y_{n})\\
\vdots & \vdots & \vdots\\
y_{n}'(t) & = & f_{n}(t,y_{1},\ldots,y_{n})\end{eqnarray*}
 z začetnim pogojem \begin{eqnarray*}
y_{1}(t_{0}) & = & y_{1}\\
y_{2}(t_{0}) & = & y_{2}\\
\vdots & \vdots & \vdots\\
y_{n}(t_{0}) & = & y_{n}\end{eqnarray*}


Numerične metode, ki jih uporabimo za reševanje sistemov, so praktično
enake kot za eno samo enačbo. Paziti moramo le, da namesto skalarjev
uporabljamo vektorske količine. Poglejmo si, kako \href{http://en.wikipedia.org/wiki/Runge-Kutta}{metodo
Runge-Kutta} priredimo za sisteme enačb. Na intervalu na katerem
želimo poiskati rešitev izberemo enakomerno razporejene delilne točke
\[
t_{0},t_{1}=t_{0}+h,\ t_{2}=t_{1}+h,\ \ldots,\ t_{n}=t_{n-1}+h\]
 in $h=\frac{t_{0}-t_{n}}{n}$.

Približke za vrednosti rešitve $\mathbf{y}_{k}=\mathbf{y}(t_{k})$
v delilnih točkah izračunamo z zaporedjem približkov \begin{eqnarray*}
\mathbf{k}_{1} & = & h\ \mathbf{f}(t_{k},\mathbf{y}_{k})\\
\mathbf{k_{2}} & = & h\ \mathbf{f}(t_{k}+\frac{h}{2},\mathbf{y}_{k}+\frac{\mathbf{k}_{1}}{2})\\
\mathbf{k}_{3} & = & h\ \mathbf{f}(t_{k}+\frac{h}{2},\mathbf{y}_{k}+\frac{\mathbf{k}_{2}}{2})\\
\mathbf{k}_{4} & = & h\ \mathbf{f}(t_{k}+h,\mathbf{y}_{k}+\mathbf{k}_{3})\\
\mathbf{y}_{k+1} & = & \frac{1}{6}(\mathbf{k_{1}}+\mathbf{k_{2}}+\mathbf{k_{3}}+\mathbf{k_{4}}),\end{eqnarray*}
kjer so $\mathbf{y}_{k}$ in $\mathbf{k}_{1},\mathbf{k}_{2},\mathbf{k}_{3},\mathbf{k}_{4}$
vektorji v $\mathbb{R}^{n}$ s komponentami $(y_{1}(t_{k}),y_{2}(t_{k}),\ldots,y_{n}(t_{k}))$
funkcija \[
\mathbf{f}:\mathbb{R}\times\mathbb{R}^{n}\to\mathbb{R}^{n}\]
 pa vektorska funkcija sestavljena iz desnih strani enačb $\mathbf{f}(t,\mathbf{y})=(f_{1}(t,y_{1},\ldots,y_{n}),\ldots,f_{n}(t,y_{1},\ldots,y_{n}))$.
Funkcija \texttt{rk4.m }je napisana tako, da deluje tudi za sisteme
enačb. Paziti moramo le, da so vhodni podatki primerni.
\begin{example}
Poiščimo približno rešitev za sistem Lotka-Volterra \begin{align*}
\dot{x}(t) & =x(t)(\alpha-\beta y(t))\\
\dot{y}(t) & =-y(t)(\gamma-\delta x(t)),\end{align*}
ki opisuje populacijsko dinamiko. Če želimo uporabiti rk4.m, potrebujemo
funkcijo, ki izračuna desne strani DE:

\sourcefile{lotka_volterra.m}

Ker metoda rk4 zahteva funkcijo oblike $fun(t,y)$, ustvarimo pomožno
funkcijo, ki vsebuje tudi parametre

\inputencoding{latin2}
\begin{lstlisting}
alfa=1; beta=1; gama=1; delta=1;
lv = inline('lotka_volterra(t,Y,[alfa,beta,gama,delta])','t','Y');
t0=0; tk=10; Y0=[100;100]; n=100;
[Y,t] = rk4(lv,0,[100;100],10,100);
plot(t,Y)
\end{lstlisting}
\inputencoding{utf8}
\end{example}

\subsection{Testiranje}

Napaka v programu lahko povzroči, da numerični rezultat ne konvergira
k pravilni rešitvi. Velikokrat pa se zgodi, da numerični rezultat
kljub napaki konvergira k rešitvi, vendar je zaradi napake hitrost
konvergence nižjega reda, kot bi morala biti.

Poleg običajnega testiranja, kjer preverimo pravilnost rešitve, ki
jo vrne numerična metoda, je veliko napak v programih .

Preden zaupamo svoje probleme programu rk4.m, ga moramo dodobra stestirati.

\sourcefile{test_rk4.m}


\subsection{Kje se skriva hrošč}

\textbf{Problem: }Rešitev je povsem napačna. Na primer rešitev divergira
preko vseh meja. 
\begin{itemize}
\item zmanjšamo korak in interval na katerem računamo.

\begin{itemize}
\item napaka se zmanjša a prepočasi

\begin{itemize}
\item velika napaka je mogoče posledica premajhnega reda metode, zato preverimo
red metode 
\item velika napaka je povsem normalen pojav za problem, ki ga rešujem
\end{itemize}
\end{itemize}
\item preverimo, ali je funkcija desnih strani, ki opisuje enačbo pravilno
napisana
\item preverimo samo metodo za reševanje sistema
\end{itemize}
\textbf{Problem:} Numerična rešitev prepočasi konvergira k pravilni
rešitvi.
\begin{itemize}
\item zanka ima preveč ali pa premalo korakov
\item napaka je v izračunu posameznega koraka
\item dolžina koraka je napačna
\end{itemize}

\subsubsection{Pogoste napake}

Posameznih komponent se ne računa hkrati ampak vsako v svoji zanki.
Tako napisana metoda deluje le za zelo posebne sisteme enačb. V splošnem
moramo vse komponente računati izmenično v eni sami zanki. 


\section{Orbita Zemljinega satelita}

Vsakdanje življenje postaja vse bolj odvisno od delovanja umetnih
satelitov, ki krožijo okrog našega planeta. Približne orbite satelitov
lahko enostavno izračunamo že s Keplerjevimi zakoni, pri čemer pa
predpostavimo, da je Zemlja okrogla. Vsi pa vemo, da je Zemlja kvadratna
plošča, katere vogale podpirajo štirje gigantski sloni. šalo na stran,
a Zemlja v resnici ni popolna krogla in njenega gravitacijskega polja
ne moremo opisati s poljem točkastega telesa, kot smo navajeni iz
srednješolske fizike. Pri natančnejših izračunih si zato pomagamo
z \href{http://en.wikipedia.org/wiki/Multipole_expansion}{multipolnim
razvojem} Zemljinega gravitacijskega potenciala. Več o tem si lahko
preberete v \href{http://kgb.ijs.si/fiziki/docs/geofizika/geofiz.zip}{skripti
za geofiziko}.

Če želimo določiti potek gibanja satelita, moramo poznati fizikalne
zakone, ki ga opisujejo . V našem primeru sta relevantna dva zakona:
\begin{enumerate}
\item drugi Newtonov zakon, ki opisuje, kako sila vpliva na gibanje in
\item gravitacijski zakon, ki pove kolikšna je privlačna sila Zemlje.
\end{enumerate}
Poleg tega moramo poznati položaj in hitrost satelita v nekem trenutku.
S temi \emph{začetnimi pogoji} in Newtonovim zakonom lahko gibanje
satelita povsem opišemo. Kot smo že omenili je Newton zakon podan
v obliki sistema navadnih diferencialnih enačb 2. reda. Če poznamo
še začetni položaj in hitrost, lahko problem matematično formuliramo
kot \emph{začetni problem }oziroma \emph{Cauchyjevo nalogo} za sistem
navadnih diferencialnih enačb. 


\subsection*{Izpeljava sistema diferencialnih enačb}

Enačbe za orbito satelita izpeljemo z 2. Newtonovim zakonom. Najprej
izpeljimo enačbe za primer, ko ne upoštevamo višjih popravkov. Potencial
je potem odvisen samo do razdalje od središča $R$ in je enak\[
V_{2}(R,\vartheta)=-\frac{C}{R}\]
 in Newtonove enačbe se glasijo\begin{equation}
\ddot{\mathbf{x}}=-grad\ V_{2}=-C\frac{\mathbb{\mathbf{x}}}{R\lyxmathsym{\textthreesuperior}},\label{eq:enacbe_monopol}\end{equation}
 kjer je $\mathbf{x}=(x,y,z)^{T}$ vektor položaja satelita in $C=3.986\times10^{14}m^{3}s^{-2}$
gravitacijska konstanta za Zemljin potencial.

Kvadrupolni popravek potenciala je enak \[
V_{4}(R,\vartheta)=\frac{K}{R^{3}}P_{2}(\cos\vartheta)=\frac{K}{R^{3}}(3\cos^{2}(\vartheta)-1),\]
 kjer je $\vartheta$ polarni kot, ki ustreza zemljepisni širini in
$K=1.7555\times10^{25}m^{5}s^{-2}$. Uporabimo $z=R\ \cos\vartheta$
in potencial izrazimo s spremenljivkama $R$ in $z$ \[
V_{4}(R,z)=\frac{K}{R^{3}}(3\frac{z^{2}}{R^{2}}-1)\]
Prispevek h gradientu potenciala, ki ga prinese kvadrupolni člen lahko
zapišemo kot \begin{eqnarray*}
grad\ V_{4} & = & V_{R}\left[\begin{array}{c}
R_{x}\\
R_{y}\\
R_{z}\end{array}\right]+V_{z}\left[\begin{array}{c}
0\\
0\\
1\end{array}\right]\end{eqnarray*}
Gradient $R$ je enak \[
grad\ R=\frac{\mathbf{X}}{R}=\frac{1}{R}[x,y,z]^{T}\]
in odvodi po $R$ in $z$ so enaki \begin{eqnarray*}
V_{R} & = & -3\frac{K}{R^{4}}(5\frac{z^{2}}{R^{2}}-1)\\
V_{z} & = & \frac{6Kz^{2}}{R^{5}}\end{eqnarray*}


Newtonove enačbe v kvadrupolnem potencialu se potem glasijo \begin{equation}
\ddot{\mathbf{x}}=-grad\ V_{1}-grad\ V_{4},\label{eq:newton_kvad}\end{equation}
 če podamo še začetni položaj $\mathbf{x}(0)=\mathbf{x}_{0}$ in začetno
hitrost $\dot{\mathbf{x}}(0)=\mathbf{v_{0}}$ dobimo tako imenovani
\emph{začetni problem za sistem NDE (navadnih diferencialnih enačb).}


\subsection*{Numerični izračun}

Da bi sistema Newtonovih enačb (\ref{eq:enacbe_monopol}) in (\ref{eq:newton_kvad})
rešili numerično, moramo enačbam najprej znižati red. Postopek je
sila preprost in je lepo opisan v Orlovem učbeniku. Za vsak odvod
uvedemmo novo spremenljivko\begin{eqnarray*}
x_{1}=x & y_{1}=y & z_{1}=z\\
x_{2}=\dot{x} & y_{2}=\dot{y} & z_{2}=\dot{z}\end{eqnarray*}
 Sistem enačb za spremenljivke $x_{i},y_{i},z_{i}$ zapišemo kot\begin{eqnarray*}
\dot{x}_{1}=x_{2} & \dot{y}_{1}=y_{2} & \dot{z}_{1}=z_{2}\\
\dot{x}_{2}=\ddot{x} & \dot{y}_{2}=\ddot{y} & \dot{z}_{2}=\ddot{z}\end{eqnarray*}
To je sistem prvega reda za katerega lahko uporabimo večino metod
za reševanje NDE(navadnih diferencialnih enačb). Fiziki bi rekli,
da sistem obravnavamo na faznem prostoru oziroma prostoru stanj sistema.
Stanje sistema dobimo, če položaju satelita dodamo še njegovo hitrost. 


\subsection*{Numerični izračun orbite satelita}

Poglejmo kako bi numerično rešili začetni problem za NDE, ki opisuje
gibanje satelita v Zemljinem gravitacijskem polju.

\includegraphics[bb=50bp 400bp 580bp 720bp,clip,scale=0.8]{orbita}

Vzemimo npr. \href{http://sl.wikipedia.org/wiki/Mednarodna_vesoljska_postaja}{Mednarodno
vesoljsko postajo}, ki kroži v orbiti $h_{0}\approx340km$ nad površjem
Zemlje. Naklon ravnine glede na ekvator je $\vartheta_{0}\approx51^{\circ}$.
Glede na to, da je gibanje skoraj krožno lahko hitrost izračunamo
iz višine $v_{0}\approx27700\ km/h$. Zavoljo enostavnosti bomo za
začetni poločaj vzeli takrat, ko je postaja nad ekvatorjem \begin{eqnarray*}
x & = & z=0\\
y & = & R_{0}+h_{0}.\end{eqnarray*}
 Začetni hitrostni vektor je $\mathbf{v}=v_{0}(\cos\vartheta_{0},0,\sin\vartheta_{0})$.


\subsection*{Zaključek}

Kot smo videli kvadrupolni člen povzroči precesijo. Da bi se temu
izognili, so ravnine orbit satelitov GPS pod kotom $\approx55^{\circ}$
glede na ekvator (t.i. magični kot). Pri tem kotu je namreč $(3\cos^{2}\vartheta-1)=0$
in kvadrupolni moment ne vpliva na orbito. 


\subsection{Programi in rezultati}
\end{document}

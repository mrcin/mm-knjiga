function dY=monopol(t,Y)
% funkcija  dY=monopol(t,Y)
% izračuna desne strani sistema, ki opisuje 
% gibanje satelita v monopolnem potencalu
% Y=[x1 y1 z1 x2 y2 z2 ]' 
X1=Y(1:3);
X2=Y(4:6);
C=3.986e5; % v km^3/s^2
R=norm(X1,2);
dY=[ X2; -C/R^3*X1 ];

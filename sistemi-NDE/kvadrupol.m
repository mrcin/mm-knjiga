function dY=kvadrupol(t,Y)
% funkcija  dY=kvadrupol(t,Y)
% izračuna desne strani sistema, ki opisuje 
% gibanje satelita v potencalu s kvadrupolnem popravkom
% Y=[x1 y1 z1 x2 y2 z2 ]' 
X1=Y(1:3);
X2=Y(4:6);
z=X1(3);
C=3.986e5; % v km^3/s^2
R=norm(X1,2);
% kvadrupolni popravek V=K/R^3(3z^2/R^2-1)
K=1.7555e10; % v km^5/s^2
gradR = X1/R;
gradz = [0 0 1]';
% odvoda V po R in z
VR = K/R^4*(-15*z^2/R^2+3);
Vz = 6*K*z/R^5;
gradV = VR*gradR + Vz*gradz;
dY=[ X2; -C/R^3*X1 - gradV ];

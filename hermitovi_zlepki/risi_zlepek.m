function risi_zlepek(S,x,tock);
%RISI_ZLEPEK narise kubicni zlepek
%RISI_ZLEPEK(S,x,tock);
%S je tabela koeficientov polinoma nx4
%x je tabela delilnih tock
%tock je stevilo tock risanja na posameznem podintevalu
[n,m]=size(S); % n je stevilo podintervalov
for i=1:n
  t=linspace(x(i),x(i+1),tock);
  if (mod(i,2)==0)
    barva='b';
  else
    barva='g';
  end
  plot(t,nip(S(i,:),[x(i) x(i) x(i+1) x(i+1)],t),barva);
end
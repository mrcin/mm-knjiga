function S=vrni_zlepek(T);
%VRNI_ZLEPEK izracuna koeficiente polinomov v zlepku
%S=VRNI_ZLEPEK(T)
%T je tabela (n+1)x3 (tocke, vrednosti, odvodi)
%S je tabela nx4, i-ta vrstica so koeficienti i-tega polinom

[n,m]=size(T);
S=zeros(n-1,4);
for i=1:n-1
  % int. poli na tockah x(i) in x(i+1)
  S(i,:)=hermite(T(i:i+1,:))';
end
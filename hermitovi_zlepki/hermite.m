function p=hermite(T)
% funkcija p=hermite(T) izracuna interpolacijski polinom,
% ki interpolira vrednosti funkcije in njenih odvodov
% p koeficienti Newtonovega int. polinoma
% p(1) + p(2)*(x-x(1))
%    + p(3)*(x-x(1))^2 + p(4)*(x-x(1))^2*(x-x(2)) + ...
% T je n x 3 matrika z interpolacijskimi podatki
% vrstica matrike T vsebuje [xi, fi, fi']
[n,m] = size(T); % stopnja polinoma je 2*n-1
x = T(:,1); f = T(:,2); df = T(:,3);
a = zeros(2*n,2*n); 
a(1:2:end,1) = f;
a(2:2:end,1) = f;
% X=[x1,x1,x2,x2,x3,x3,...]
X = zeros(2*n,1);
X(1:2:end) = x;
X(2:2:end) = x;
for i = 2:2*n
for j = 2:i
  if (X(i) == X(i-j+1)) % potem vzamemo odvod
    a(i,j) = df(i/2);
  else
    a(i,j) = (a(i,j-1) - a(i-1,j-1))/(X(i) - X(i-j+1));
  end
end
end
p = diag(a);
function y=nip(p,x,t)
% funkcija y=nip(p,x,t) izracuna vrednosti Newtonovega inter. polinoma
% p(1) + p(2)(x-x(1)) + p(3)(x-x(1))(x-x(2)) + ...
% v tocki t
n=length(p)-1;
y = p(n+1);
for i = n:-1:1
  y = y.*(t-x(i)) + p(i);
end
function [U]=milnica_lu(a,fun,n);
%MILNICA_LU resuje Laplaceovo enacbo z LU razcepom
%na kvadratu [-a,a]^2
%U=MILNICA_LU(a,fun,n);
%U so vrednosti funkcije na mrezi
%a je desno krajisce kvadrata
%n je stevilo notranjih tock
[A,b,robovi]=naredi_matriko(a,fun,n);
u=A\b;
U=reshape(u,n,n)';
%obrobimo z robnimi vrednostimi
U=[flipud(robovi(1,:)') [robovi(2,2:n+1);U;robovi(4,2:n+1)] flipud(robovi(3,:)')];
U=flipud(U);
x=linspace(-a,a,n+2);
clf;
hold on
enke=ones(1,n+2);
robx = [ -a*enke x a*enke fliplr(x) ];
roby = [ x  a*enke fliplr(x)  -a*enke];
plot3(robx, roby, reshape(robovi',1,4*length(robovi)),'r*')
surf(x,x,U);
axis equal
hold off


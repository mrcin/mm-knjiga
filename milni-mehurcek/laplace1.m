function [Ukon,k] = laplace(U, tol, maxk)
% Funkcija [x,k] = LAPLACE(n,b) poisce resitev sistema A u = b
% z Jacobijevo iteracijo
% U in B sta n x n matriki
% A je Laplaceova matrika v dveh dimenzijah, b je vektor, ki ga dobimo, ce vrstice B 
% eno za drugo zlozimo v stolpec, u pa dobimo na podoben nacin iz U
% tol je vrednost tolerance. Iteracija naj se konca, ko |xk-xkk|<tol 
% maxk je najvecje stevilo korakov po katerem se iteracija brezpogojno neha
[n,m] = size(U);
razlika = inf;
k=0;
Ukon = U;
while ((razlika > tol)&(k < maxk)) 
    for i=2:n-1
        for j=2:n-1
            U(i,j)=1/4*(Ukon(i,j-1)+Ukon(i,j+1)+Ukon(i-1,j)+Ukon(i+1,j));
        end
    end
    razlika = max(max(abs(U-Ukon)));
    Ukon=U;
    k = k+1;
end; %while
% ce smo presegli stevilo korakov
%if razlika > tol
%    U=NaN;
%end

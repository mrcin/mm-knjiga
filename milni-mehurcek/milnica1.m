function [U,k] = milnica(a,n,fun,tol,maxk)
% funkcija [U,k] = milnica(a,n,fun, tol, maxk) izracuna 
%obliko milnice na kvadratu [-a,a]^2, 
% ce so pobane robne vrednosti
% n+2 je stevilo delilnih tock na eni koordinatni osi
% fun je funkcija 1 spremenljivke, ki vrne vrednosti na robu
% in sicer matriko s stirimi vrsticami [levo; zgoraj;desno;spodaj]
% parametre maxk in tol podamo funkciji laplace

% Sestavimo desne strani
xx = linspace(-a,a,n+2);
x = xx(2:n+1);
enke = ones(1,n+2);
rp = feval(fun,xx,a);

Up=zeros(n+2);
%B = zeros(n);
%B(:,1) = -rp(1,2:n+1)';
%B(:,n) = -rp(3,2:n+1)';
%B(1,:) = B(1,:) - rp(2,2:n+1);
%B(n,:) = B(n,:) - rp(4,2:n+1);
Up(:,1) = rp(1,:)';
Up(:,n+2) = rp(3,:)';
Up(1,2:n+1) = rp(2,2:n+1);
Up(n+2,2:n+1) = rp(4,2:n+1);
[U,k] = laplace1(Up,tol,maxk);
robx = [ -a*enke xx a*enke xx ];
roby = [ xx  a*enke xx  -a*enke];
%obrobimo U z robom
%U=flipud(U);
%U=[flipud(rp(1,:))' [rp(2,2:end-1);U;rp(4,2:end-1)] flipud(rp(3,:)')];
U=flipud(U);
clf
hold on
plot3(robx, roby, reshape(rp',1,4*length(rp)),'r*');
surf(xx,xx,U);
xlabel('X')
axis equal
hold off

function [U,k] = milnica(a,n,fun,tol,maxk,metoda)
% funkcija [U,k] = milnica(a,n,fun,tol,maxk,metoda) izracuna 
%obliko milnice na kvadratu [-a,a]^2, 
% ce so podane robne vrednosti
% n+2 je stevilo delilnih tock na eni koordinatni osi
% fun je funkcija spremenljivke x in a, ki vrne vrednosti na robu
% in sicer matriko s stirimi vrsticami [levo; zgoraj;desno;spodaj]
% parametre maxk in tol podamo funkciji laplace
%metoda je stikalo, ki pove ali zelimo:
%'Jacobi'=Jacobijevo iteracijo
%'Gauss-Seidel'=Gauss-Seidelovo iteracijo
%'SOR'=SOR iteracijo

% Dolocimo robne vrednosti
x = linspace(-a,a,n+2);
robovi = feval(fun,x,a);
n2=floor(n/2)
Up=zeros(n+2);
Up(:,1) = robovi(1,:)';
Up(:,n+2) = robovi(3,:)';
Up(1,2:n+1) = robovi(4,2:n+1);
Up(n+2,2:n+1) = robovi(2,2:n+1);
Up(n2,n2)=1;
switch lower(metoda)
	case {'jacobi'}
		[U,k]=laplace_jac(Up,tol,maxk);
	case {'gauss-seidel'}
		[U,k]=laplace_gs(Up,tol,maxk);
	case {'sor'}
		w=input('Podajte parameter w za SOR metodo: ');
		[U,k]=laplace_sor(Up,w,tol,maxk);
end

%enke=ones(1,n+2);
%robx = [ -a*enke x a*enke x];
%roby = [ x  a*enke x  -a*enke];
%robz=reshape(robovi',1,4*(n+2));
%tocke=[robx',roby',robz']; 
clf;
hold on;
%plot3(robx, roby, robz,'r*')%matlab
mesh(x,x,U);
%plot3(tocke,'with linespoints')
%axis equal
hold off

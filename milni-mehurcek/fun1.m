function RP = fun(x)
% funkcija vrne vrednosti na robu kvadrata [a,b]^2
% vrednosti na stranicah zlozi v vrstice
%levo, zgoraj desno spodaj
n = length(x);
RP = [ zeros(1,n); 1-x.^2; 2-2*x.^2; zeros(1,n)];

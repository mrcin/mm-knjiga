function Z = milnica_movie(a,n,fun,slik)
%MILNICA_MOVIE zgradi film o iteraciji Jacobijeve metode
%Z=MILNICA_MOVIE(a,n,fun,slik)
%Z je tenzor slik (po nivojih)
%a je podatek o stranici kvadrata (2*a)
%n je stevilo tock na mrezi v eni smeri
%fun je funkcija, ki vraca robne vrednosti
%slik je stevilo slik v film
xx = linspace(-a,a,n+2);
rp = feval(fun,xx,a);
Up=zeros(n+2);
Up(:,1) = rp(1,:)';
Up(:,n+2) = rp(3,:)';
Up(1,2:n+1) = rp(4,2:n+1);
Up(n+2,2:n+1) = rp(2,2:n+1);
Z=zeros(n+2,n+2,slik);
for i=1:slik
  [U,k]=laplace_jac(Up,1e-3,i);
  Z(:,:,i)=U;
end;
clf;
for i=1:slik 
  surf(xx,xx,Z(:,:,i));
  view([37,24]);
  pause(0.005);
end

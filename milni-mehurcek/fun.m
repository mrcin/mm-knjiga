function RP = fun(x,a)
%FUN vraca robne vrednosti na kvadratu [-a,a]x[-a,a]
%RP=FUN(x,a)
%RP je matrika robnih vrednosti
%vrednosti na stranicah so v vrsticah 
%v prvi vrstico levi rob, v drugi zgornji
%v tretji desni, v cetrti spodnji
n = length(x);
%RP = [ zeros(1,n); 1-x.^2; 2-2*x.^2; zeros(1,n)];
RP=[zeros(1,n);a^2-x.^2;(a^2-x.^2).*x;zeros(1,n)];
%RP=zeros(4,n);
%RP=[zeros(3,n);x.^2-a^2];
%RP=[a^2-x.^2;zeros(1,n);a^2-x.^2;x.^2-a^2];
%RP=[zeros(2,n);x.^2-a^2;a^2-x.^2];

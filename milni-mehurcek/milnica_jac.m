function [U,k] = milnica_jac(a,n,fun,tol,maxk)
%MILNICA vrne vrednosti resitve Laplaceove enacbe
%[U,k]=MILNICA(a,n,fun,tol,maxk)
%U je matrika vrednosti, k stevilo korakov iteracije
%a je podatek o stranici kvadrata (2*a)
%n stevilo notranjih delilnih tock na mrezi
%fun je ime funkcije, ki vraca robne vrednosti
%tol in maxk sta parametra za iteracijo

xx = linspace(-a,a,n+2);
x = xx(2:n+1);
enke = ones(1,n+2);
rp = feval(fun,xx,a);

Up=zeros(n+2);
Up(:,1) = rp(1,:)';
Up(:,n+2) = rp(3,:)';
Up(1,2:n+1) = rp(4,2:n+1);
Up(n+2,2:n+1) = rp(2,2:n+1);
[U,k] = laplace_jac(Up,tol,maxk);
robx = [ -a*enke xx a*enke xx ];
roby = [ xx  a*enke xx  -a*enke];
robz=reshape(rp',1,4*length(rp));
%U=flipud(U);
clf
hold on
plot3(robx, roby, reshape(rp',1,4*length(rp)),'r*');
surf(xx,xx,U);
xlabel('X');
ylabel('Y');
axis equal
hold off

function [U,k] = laplace(B, tol, maxk)
% Funkcija [x,k] = LAPLACE(n,b) poisce resitev sistema A u = b
% z Jacobijevo iteracijo
% U in B sta n x n matriki
% A je Laplaceova matrika v dveh dimenzijah, b je vektor, ki ga dobimo, ce vrstice B 
% eno za drugo zlozimo v stolpec, u pa dobimo na podoben nacin iz U
% tol je vrednost tolerance. Iteracija naj se konca, ko |xk-xkk|<tol 
% maxk je najvecje stevilo korakov po katerem se iteracija brezpogojno neha
[n,m] = size(B);

xk = B;
razlika = inf;
k=0;
nicle = zeros(n+2,1);
Uk = B;
while ((razlika > tol)&(k < maxk)) 
    % izracuna del vektorja u, ki pripada prvi vrstici 
    % element levo zgoraj 
    U(1,1)=-1/4*(B(1,1)-Uk(1,2)-Uk(2,1));
    for i=2:n-1
	U(1,i) = -1/4*(B(1,i)-Uk(1,i-1)-Uk(1,i+1)-Uk(2,i));
    end
    % element desno zgoraj
    U(1,n)= -1/4*(B(1,n)-Uk(1,n-1)-Uk(2,n));
    %srednji del matrike
    for i=2:n-1
	U(i,1) = -1/4*(B(i,1)-Uk(i-1,1)-Uk(i+1,1)-Uk(i,2));
	for j=2:n-1
	    U(i,j) = -1/4*(B(i,j)-Uk(i-1,j)-Uk(i,j+1)-Uk(i+1,j)-Uk(i,j-1));
	end
	U(i,n) = -1/4*(B(i,n)-Uk(i-1,n)-Uk(i,n-1)-Uk(i+1,n));
    end
    % levo spodaj
    U(n,1)= -1/4*(B(n,1)-Uk(n,n-1)-Uk(n-1,n));
    % izracuna se spodnjo vrstico
    for i=2:n-1
	U(n,i) = -1/4*(B(n,i)-Uk(n,i-1)-Uk(n,i+1)-Uk(n-1,i));
    end
    % desno spodaj
    U(n,n)= -1/4*(B(n,n)-Uk(n,n-1)-Uk(n-1,n));
    razlika = max(max(abs(U-Uk)));
    Uk=U;
    k = k+1;
end; %while
% ce smo presegli stevilo korakov
%if razlika > tol
%    U=NaN;
%end

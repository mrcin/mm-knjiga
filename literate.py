#!/usr/bin/python
# vim: set fileencoding=utf-8 :
"""
Skripta izvleče programsko kodo iz LaTeX dokumenta. Programska koda je lahko napisana v kateremkoli 
programskem jeziku in je lahko vstavljena v dokument v poljubnem vrstnem redu. Koda je karkoli kar je v LaTeX okolju lstlisting. S parametrom name=, ki ga podamo okolju lstlisting, lahko določimo ime kosa programske kode. Če parameter name ni določen, se koda shrani v datoteko izstim imenom kot dokument in končnico .m (octave). 
Kosi kode, ki vsebujejo piko, se avtomatično zapišejo v datoteke.
Primer
\\begin{lstlisting}[name=koda.m]
% octave koda se bo shranila v datoteko koda.m
print("Hello World!")
\\end{lstlisting}
"""
import sys
import re
from os.path import basename, splitext
KODE_ENV = r'(lstlisting|code|hiden|octave)'
KODA_IME_RE = re.compile(r'\\begin\{' + KODE_ENV + r'\}\[.*name=(.*?),*\](.*?)\\end\{'+ KODE_ENV + '\}',re.DOTALL)
KODA_RE = re.compile(r'\\begin\{' + KODE_ENV + r'\}([^\[].*?)\\end\{' + KODE_ENV + '\}',re.DOTALL)
FILE_KOS_RE  = re.compile(r'\.')
REF_RE = re.compile(r'\\coderef\{(.*?)\}') # regularni izraz zavključevanje kode

def extract_code(filename):
	"""
	Funkcija izvleče kose kode iz dokumenta in jih shrani v ustrezne datoteke.
	"""
	texfile  = open(filename,'r')
	tex = texfile.read() # preberemo ves tekst
	kosi_ime = KODA_IME_RE.findall(tex) # poiščemo imenovane kose
	kosi_noname = KODA_RE.findall(tex) # poiščemo neimenovane kose
	kosi = {} # kose shranimo v slovar
	for kos in kosi_ime:
		# preveri, ali je začetek lstlistings
		print kos
		if kosi.has_key(kos[0]):
			kosi[kos[1]] += kos[2] # dodamo kodo obstoječemu kosu
		else:
			kosi[kos[1]] = kos[2] # ustvarimo nov kos v slovarju
	if len(kosi_noname)>0:
		# združimo generične kose brez oznake
		kosi[splitext(basename(filename))[0]+".m"] ="".join(kosi_noname)
	# print kosi
	# vključimo linke v kose
	# toliko časa se sprehajamo po kosih, dokler ne počistimo vseh referenc
	# \coderef{...}
	kosi_final = {} # slovar za obdelane kose
	while len(kosi)>0:
		for kos in kosi:
			refs = REF_RE.findall(kosi[kos])
			print refs
			for ref in refs:
				# zamenjamo \coderef z vsebino referenčnega kosa 
				kosi[kos]=re.sub(r'\\coderef\{%s\}'%ref,kosi[ref],kosi[kos])
			print kosi[kos]
		for kos in kosi.keys():
			if not re.search(r'\\coderef{(.*)}',kosi[kos]):
				# ce v kosu ni več referenc, ga spravimo v končni seznam
				kosi_final[kos]=kosi[kos]
				del(kosi[kos])
	# konec while
	# kose zložimo nazaj v datoteke
	for kos in kosi_final:
		if FILE_KOS_RE.search(kos):
			print ("Pišem v datoteko %s" % kos) 
			f = open(kos,'w')
			f.write(kosi_final[kos])
			f.close
# TODO dodaj možnost zamenjave vrstnega reda in vključevanja kode
if __name__=="__main__":
	if len(sys.argv)>1:
		filename = sys.argv[1]
		extract_code(filename)
	else:
		print "Usage: python literate.py ime_dokumenta.tex"

#LyX 1.6.4 created this file. For more info see http://www.lyx.org/
\lyxformat 345
\begin_document
\begin_header
\textclass book
\use_default_options true
\language slovene
\inputencoding utf8
\font_roman default
\font_sans default
\font_typewriter default
\font_default_family default
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_amsmath 1
\use_esint 1
\cite_engine basic
\use_bibtopic false
\paperorientation portrait
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\defskip medskip
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\author "" 
\author "" 
\end_header

\begin_body

\begin_layout Chapter
Navadne diferencialne enačbe
\end_layout

\begin_layout Abstract
\begin_inset FormulaMacro
\renewcommand{\rmd}{\mathrm{d}}
\end_inset


\end_layout

\begin_layout Abstract
Veliko pojavov lahko opišemo z navadnimi diferencialnimi enačbami.
 Sem spadajo predvsem dinamični sistemi, ki opisujejo spremembe opazljivk
 v odvisnosti od časa.
 Diferencialne enačbe omogočajo, da za opis uporabimo „lokalne zakone“,
 ki so relativno enostavni.
 Najlepši primer „lokalnega zakona“ je drugi Newtonov zakon, ki opisuje
 gibanje točkastega telesa pod vplivom sile in sicer preavi, da je pospešek
 sorazmeren sili, ki deluje na točkasto telo.
 Zakon lahko zapišemo z diferencialno enačbo 2.
 reda
\begin_inset Formula \begin{equation}
a=\frac{\rmd^{2}x(t)}{\rmd t^{2}}=\frac{F}{m}.\label{eq:Newton2}\end{equation}

\end_inset

Če želimo določiti trajektorijo točkastega telesa, moramo poznati še začetni
 položaj in začetno hitrost v določenem trenutku.
 to pomeni, da imamo poleg diferencialne enačbe 
\begin_inset CommandInset ref
LatexCommand eqref
reference "eq:Newton2"

\end_inset

 podana še dva začetna pogoja
\begin_inset Formula \begin{align*}
x(t_{0}) & =x_{0}\\
\dot{x}(t_{0}) & =v_{0}.\end{align*}

\end_inset

Tako zastavljeni problem ima enolično rečitev in ga imenujemo 
\emph on
začetni problem 
\emph default
oziroma 
\emph on
Cauchyjeva naloga 
\emph default
za diferencialno enačbo
\emph on
.

\emph default
 Omeniti velja, da za diferencialne enačbe ne rešujemom vedno začetnega
 problema.
 Velikokrat je treba poiskati rešitev 
\emph on
 robnega problema,
\emph default
 ki se ga rešuje povsem drugače kot začetnega.
 Predtavljajmo si navaden navpičen met žogice.
 Vprašanje, ki ga zastavlja robni problem je, kako moramo vreči žogico,
 da bo po določenem času padla na tla.
 Namesto začetnih pogojev imamo sedaj dva robna pogoja
\begin_inset Formula \[
x(0)=0\quad\text{in }x(t_{0})=0.\]

\end_inset

V tem poglavju se bomo osredotočili na reševanje začetnega problema, robne
 probleme si bomo ogledali v naslednjem poglavju.
\end_layout

\begin_layout Section
Numerične metode za začetni problem
\end_layout

\begin_layout Standard
Metode za reševanje začetnega problema temeljijo na preprosti ideji.
 Sposodimo si fizikalne izraze in si poglejmo, kaj v resnici pove diferencialna
 enačba
\begin_inset Formula \begin{equation}
y'(t)=f(t,y(t))\label{eq:DE1}\end{equation}

\end_inset

o obnašanju opazljivke 
\begin_inset Formula $y(t)$
\end_inset

.
 Če poznamo vrednosti 
\begin_inset Formula $t$
\end_inset

 in 
\begin_inset Formula $y$
\end_inset

, lahko iz enačbe 
\begin_inset CommandInset ref
LatexCommand eqref
reference "eq:DE1"

\end_inset

 izračunamo vrednost odvoda.
 Pri začetnem problemu imamo v času 
\begin_inset Formula $t_{0}$
\end_inset

 podan začetni pogoj 
\begin_inset Formula \[
y(t_{0})=y_{0}\]

\end_inset

in lahko iz enačbe 
\begin_inset CommandInset ref
LatexCommand eqref
reference "eq:DE1"

\end_inset

 izračunamo tudi vrednost odvoda 
\begin_inset Formula $y'(t_{0})$
\end_inset

.
 Diferencialna enačba nam torej za 
\begin_inset Formula $t_{0}$
\end_inset

 pove, kako hitro se vrednost 
\begin_inset Formula $y(t)$
\end_inset

 spreminja.
 Za dovolj majhen 
\begin_inset Formula $h$
\end_inset

 lahko torej dovolj dobro ocenimo vrednost funkcije v 
\begin_inset Formula $y(t_{0}+h)$
\end_inset

.
 Ko imamo približek za vrednost 
\begin_inset Formula $y(t_{0}+h)$
\end_inset

 lahko zopet iz diferencialne enačbe izračunamo odvod v 
\begin_inset Formula $t_{0}+h$
\end_inset

 in spet ocenimo vrednost v bližnji točki 
\begin_inset Formula $y(t_{0}+2h)$
\end_inset

.
 Tako lahko korak za korakom računamo nove približke, ki vedno bolj oddaljeni
 od začetnega časa 
\begin_inset Formula $t_{0}$
\end_inset

.
 Korak je lahko fiksen ali pa ga adaptivno spreminjamo glede na oceno napake.
 V nadaljevanju si bomo ogledali metodo Runge-Kutta reda 4, ki jo bomo uporablja
li za numerične izračune v tej knjigi.
\end_layout

\begin_layout Subsection
Metoda Runge-Kutta 4
\end_layout

\begin_layout Standard
V knjigi bomo uporabili metodo 
\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
href{http://en.wikipedia.org/wiki/Runge-Kutta}{
\end_layout

\end_inset

metodo Runge-Kutta
\begin_inset ERT
status collapsed

\begin_layout Plain Layout

}
\end_layout

\end_inset

 reda 4.
 Metoda je relativno enostavna, po drugi strani pa je dovolj visokega reda,
 da lahko z njo rešujemo tudi bolj zahtevne probleme.
 Rešujemo začetni problem za diferencialno enačbo 1.
 reda
\begin_inset Formula \[
y'(t)=f(t,y)\]

\end_inset

z začetnim pogojem 
\begin_inset Formula $y(t_{0})=y_{0}$
\end_inset

.
 Metoda računa zaporedne približke 
\begin_inset Formula $y_{k}$
\end_inset

 v ekvidistančnih točkah
\begin_inset Formula \[
t_{0},t_{0}+h,t_{0}+2h,\ldots.\]

\end_inset

Naslednji približek 
\begin_inset Formula $y_{k}$
\end_inset

 izračunamo iz prejšnjega s formulo
\begin_inset Formula \begin{align*}
k_{1} & =h\, f(t_{k-1},y_{k-1})\\
k_{2} & =h\, f(t_{k-1}+\frac{h}{2}+y_{k-1}+\frac{k_{1}}{2})\\
k_{3} & =h\, f(t_{k-1}+\frac{h}{2}+y_{k-1}+\frac{k_{2}}{2})\\
k_{4} & =h\, f(t_{k-1}+h+y_{k-1}+k_{3})\\
y_{k} & =y_{k-1}+\frac{1}{6}(k_{1}+k_{2}+k_{3}+k_{4}).\end{align*}

\end_inset

Vse skupaj lahko enostavno zapakiramo v 
\emph on
octave 
\emph default
funkcijo
\end_layout

\begin_layout Quote
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
sourcefile{rk4.m}
\end_layout

\end_inset


\end_layout

\begin_layout Subsection
Metode prirejene za sisteme NDE
\end_layout

\begin_layout Standard
Rešujemo torej začetni problem za sistem navadnih diferencialnih enačb prvega
 reda: 
\begin_inset Formula \begin{eqnarray*}
y_{1}'(t) & = & f_{1}(t,y_{1},\ldots,y_{n})\\
y_{2}'(t) & = & f_{2}(t,y_{1},\ldots,y_{n})\\
\vdots & \vdots & \vdots\\
y_{n}'(t) & = & f_{n}(t,y_{1},\ldots,y_{n})\end{eqnarray*}

\end_inset

 z začetnim pogojem 
\begin_inset Formula \begin{eqnarray*}
y_{1}(t_{0}) & = & y_{1}\\
y_{2}(t_{0}) & = & y_{2}\\
\vdots & \vdots & \vdots\\
y_{n}(t_{0}) & = & y_{n}\end{eqnarray*}

\end_inset


\end_layout

\begin_layout Standard
Numerične metode, ki jih uporabimo za reševanje sistemov, so praktično enake
 kot za eno samo enačbo.
 Paziti moramo le, da namesto skalarjev uporabljamo vektorske količine.
 Poglejmo si, kako 
\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
href{http://en.wikipedia.org/wiki/Runge-Kutta}{
\end_layout

\end_inset

metodo Runge-Kutta
\begin_inset ERT
status collapsed

\begin_layout Plain Layout

}
\end_layout

\end_inset

 priredimo za sisteme enačb.
 Na intervalu na katerem želimo poiskati rešitev izberemo enakomerno razporejene
 delilne točke 
\begin_inset Formula \[
t_{0},t_{1}=t_{0}+h,\ t_{2}=t_{1}+h,\ \ldots,\ t_{n}=t_{n-1}+h\]

\end_inset

 in 
\begin_inset Formula $h=\frac{t_{0}-t_{n}}{n}$
\end_inset

.
\end_layout

\begin_layout Standard
Približke za vrednosti rešitve 
\begin_inset Formula $\mathbf{y}_{k}=\mathbf{y}(t_{k})$
\end_inset

 v delilnih točkah izračunamo z zaporedjem približkov 
\begin_inset Formula \begin{eqnarray*}
\mathbf{k}_{1} & = & h\ \mathbf{f}(t_{k},\mathbf{y}_{k})\\
\mathbf{k_{2}} & = & h\ \mathbf{f}(t_{k}+\frac{h}{2},\mathbf{y}_{k}+\frac{\mathbf{k}_{1}}{2})\\
\mathbf{k}_{3} & = & h\ \mathbf{f}(t_{k}+\frac{h}{2},\mathbf{y}_{k}+\frac{\mathbf{k}_{2}}{2})\\
\mathbf{k}_{4} & = & h\ \mathbf{f}(t_{k}+h,\mathbf{y}_{k}+\mathbf{k}_{3})\\
\mathbf{y}_{k+1} & = & \frac{1}{6}(\mathbf{k_{1}}+\mathbf{k_{2}}+\mathbf{k_{3}}+\mathbf{k_{4}}),\end{eqnarray*}

\end_inset

kjer so 
\begin_inset Formula $\mathbf{y}_{k}$
\end_inset

 in 
\begin_inset Formula $\mathbf{k}_{1},\mathbf{k}_{2},\mathbf{k}_{3},\mathbf{k}_{4}$
\end_inset

 vektorji v 
\begin_inset Formula $\mathbb{R}^{n}$
\end_inset

 s komponentami 
\begin_inset Formula $(y_{1}(t_{k}),y_{2}(t_{k}),\ldots,y_{n}(t_{k}))$
\end_inset

 funkcija 
\begin_inset Formula \[
\mathbf{f}:\mathbb{R}\times\mathbb{R}^{n}\to\mathbb{R}^{n}\]

\end_inset

 pa vektorska funkcija sestavljena iz desnih strani enačb 
\begin_inset Formula $\mathbf{f}(t,\mathbf{y})=(f_{1}(t,y_{1},\ldots,y_{n}),\ldots,f_{n}(t,y_{1},\ldots,y_{n}))$
\end_inset

.
 Funkcija 
\family typewriter
rk4.m 
\family default
je napisana tako, da deluje tudi za sisteme enačb.
 Paziti moramo le, da so vhodni podatki primerni.
\end_layout

\begin_layout Example
Poiščimo približno rešitev za sistem Lotka-Volterra 
\begin_inset Formula \begin{align*}
\dot{x}(t) & =x(t)(\alpha-\beta y(t))\\
\dot{y}(t) & =-y(t)(\gamma-\delta x(t)),\end{align*}

\end_inset

ki opisuje populacijsko dinamiko.
 Če želimo uporabiti rk4.m, potrebujemo funkcijo, ki izračuna desne strani
 DE:
\end_layout

\begin_layout Example
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
sourcefile{lotka_volterra.m}
\end_layout

\end_inset


\end_layout

\begin_layout Example
Ker metoda rk4 zahteva funkcijo oblike 
\begin_inset Formula $fun(t,y)$
\end_inset

, ustvarimo pomožno funkcijo, ki vsebuje tudi parametre
\end_layout

\begin_layout Example
\begin_inset listings
inline false
status open

\begin_layout Plain Layout

alfa=1; beta=1; gama=1; delta=1;
\end_layout

\begin_layout Plain Layout

lv = inline('lotka_volterra(t,Y,[alfa,beta,gama,delta])','t','Y');
\end_layout

\begin_layout Plain Layout

t0=0; tk=10; Y0=[100;100]; n=100;
\end_layout

\begin_layout Plain Layout

[Y,t] = rk4(lv,0,[100;100],10,100);
\end_layout

\begin_layout Plain Layout

plot(t,Y)
\end_layout

\end_inset


\end_layout

\begin_layout Subsection
Testiranje
\end_layout

\begin_layout Standard
Napaka v programu lahko povzroči, da numerični rezultat ne konvergira k
 pravilni rešitvi.
 Velikokrat pa se zgodi, da numerični rezultat kljub napaki konvergira k
 rešitvi, vendar je zaradi napake hitrost konvergence nižjega reda, kot
 bi morala biti.
\end_layout

\begin_layout Standard
Poleg običajnega testiranja, kjer preverimo pravilnost rešitve, ki jo vrne
 numerična metoda, je veliko napak v programih .
\end_layout

\begin_layout Standard
Preden zaupamo svoje probleme programu rk4.m, ga moramo dodobra stestirati.
\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
sourcefile{test_rk4.m}
\end_layout

\end_inset


\end_layout

\begin_layout Subsection
Kje se skriva hrošč
\end_layout

\begin_layout Standard

\series bold
Problem: 
\series default
Rešitev je povsem napačna.
 Na primer rešitev divergira preko vseh meja.
 
\end_layout

\begin_layout Itemize
zmanjšamo korak in interval na katerem računamo.
\end_layout

\begin_deeper
\begin_layout Itemize
napaka se zmanjša a prepočasi
\end_layout

\begin_deeper
\begin_layout Itemize
velika napaka je mogoče posledica premajhnega reda metode, zato preverimo
 red metode 
\end_layout

\begin_layout Itemize
velika napaka je povsem normalen pojav za problem, ki ga rešujem
\end_layout

\end_deeper
\end_deeper
\begin_layout Itemize
preverimo, ali je funkcija desnih strani, ki opisuje enačbo pravilno napisana
\end_layout

\begin_layout Itemize
preverimo samo metodo za reševanje sistema
\end_layout

\begin_layout Standard

\series bold
Problem:
\series default
 Numerična rešitev prepočasi konvergira k pravilni rešitvi.
\end_layout

\begin_layout Itemize
zanka ima preveč ali pa premalo korakov
\end_layout

\begin_layout Itemize
napaka je v izračunu posameznega koraka
\end_layout

\begin_layout Itemize
dolžina koraka je napačna
\end_layout

\begin_layout Subsubsection
Pogoste napake
\end_layout

\begin_layout Standard
Posameznih komponent se ne računa hkrati ampak vsako v svoji zanki.
 Tako napisana metoda deluje le za zelo posebne sisteme enačb.
 V splošnem moramo vse komponente računati izmenično v eni sami zanki.
\end_layout

\begin_layout Standard
\begin_inset CommandInset include
LatexCommand include
filename "sistemi-NDE/sateliti.lyx"

\end_inset


\end_layout

\end_body
\end_document
